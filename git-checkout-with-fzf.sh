#!/bin/bash

# source: https://phelipetls.github.io/posts/favorite-custom-git-commands/#git-cb
# deps: git/fzf

set -eu

branch=$(git recent --format '%(refname:short)' | fzf --height 40% --preview 'git log --oneline {}')

if [[ -n "$branch" ]]; then
	git checkout "$branch"
fi
