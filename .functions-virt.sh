#!/bin/sh
#
# .functions-virt.sh: qemu/kvm/virt-manager only related functions

# qemu - kvm
#
# quick and dirty way to start a virtual machine
# providing an .img file as $1 arg
start_vm() {
	qemu-kvm -hda "${1}" -m 1G -device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::5555-:22
}
