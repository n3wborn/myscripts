#!/bin/bash

src="${HOME}/prog/git/neovim"
install_prefix="${HOME}/prog/nvim/"
build_type='Release'
ncpu=$(grep -c processor /proc/cpuinfo)

function pull {
	git restore . && git pull
}

function get_dir {
	[ -d "${src}" ] || mkdir -p "${src}"
	cd "${src}" || exit 1
}

function install {
	get_dir &&
		pull &&
		make -j "${ncpu}" CMAKE_BUILD_TYPE="${build_type}" CMAKE_INSTALL_PREFIX="${install_prefix}" &&
		make install &&
		exit 0 || exit 1
}

function reinstall {
	get_dir &&
		make distclean &&
		pull &&
		make -j "${ncpu}" CMAKE_BUILD_TYPE="${build_type}" CMAKE_INSTALL_PREFIX="${install_prefix}" &&
		make install
}

function backup {
	[ -f "${install_prefix}" ] &&
		cp -v "${install_prefix}" "${install_prefix}.old"
}

cat <<-EOF
	############################################################
	                    nvim-nightly-build
	############################################################

	src                   = "${src}"
	CMAKE_INSTALL_PREFIX  = "${build_type}"
	CMAKE_BUILD_TYPE      = "${install_prefix}"

	############################################################
EOF

while getopts ":f" arg; do
	case "${arg}" in
	f) reinstall ;;
	*) install ;;
	esac
done
