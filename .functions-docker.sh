#!/bin/sh
#
# .functions.docker.sh : Docker-only shell functions

# Print docker volume names
docker_volumes_names() {
	docker volume ls --format "{{.Name}}"
}

# Show docker stats for humans
docker_top() {
	docker stats "$(docker ps | awk '{if(NR>1) print $NF}')"
}

# Show each running container ID/name
docker_cont_names() {
	docker ps | awk "{print ${1} \"\t\" $NF}"
}
