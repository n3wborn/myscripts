#!/bin/bash -

# image-resize.sh : modifie à la volée la taille des images .
# onliner ok :
# for  i in *.jpg; do convert  "${i}" -resize 450x $(basename -s .jpg "${i}")--450px.jpg ; done

# NOTES:
#
#   C'est en phase de test !
#   Le onliner marche, il a été testé en l'état mais le script lui-même n'a pas eu le temps de faire
#   ses preuves du tout.

# TODO: gérer les différentes extensions en utilisant les arguments fournis au script

ext='.jpg'

for file in "${@}"; do
	out=$(basename -s ${ext} "${file}")
	convert "${file}" -resize 450x "${out}" --450px.jpg
done
