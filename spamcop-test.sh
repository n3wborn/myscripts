#!/bin/bash -

function cutter() {
    echo $1 | cut -d "." -f$2
}

function reverse() {

    a=$(cutter $1 1)
    b=$(cutter $1 2)
    c=$(cutter $1 3)
    d=$(cutter $1 4)

    echo $d.$c.$b.$a
}

if host $(reverse $1).bl.spamcop.net > /dev/null; then
    echo "$1 listed in bl.spamcop.net"
else
    echo "$1 not listed in bl.spamcop.net"
fi

