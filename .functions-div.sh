#!/bin/sh
#
# ~/.functions-div.sh : where lives "orphan" functions

# shorter (smarter ?) whois
my_whois() {
	whois "$1" | grep -iE 'inetnum:|NetRange:|CIDR:|Organization:|OrgName:|netname:|domain:|status:|nserver:|registrar:|anonymous:|descr:|country:'
}

# kind of website vacuum
my_aspi() {
	wget -r -k -np --user-agent=Firefox "$@"
}

# just show my public ip
my_ip() {
	curl -s ifconfig.me | cut -d '%' -f 1
	# curl -s https://ipinfo.io/ip ?
}

# geoip request (geoip seemed at the time it wrote it)
# NOTE: jq required (Command-line JSON processor)
my_geoip() {
	curl -s https://ipvigilante.com/"$(curl -s https://ipinfo.io/ip)" |
		jq '.data.latitude, .data.longitude, .data.city_name, .data.country_name'
}

my_random_str() {
	tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c "${1:-12}"
	echo
}

# get total ram usage of process where process name = processname
# ex: get_ram_from_processname firefox
get_ram_from_processname() {
	ps -ely | awk -v processname="$1" '$13 == processname' | awk '{SUM += $8/1024} END {print SUM}' | cut -d '.' -f1
}

get_ram_from_pid() {
	ps -ely -p "$1" | awk '{SUM += $8/1024} END {print SUM}' | cut -d '.' -f1
}

# ex: get_ram_from_user $USER
get_ram_from_user() {
	ps -elyf | awk -v user="$1" '$2 == user' | awk '{SUM += $8/1024} END {print SUM}' | cut -d '.' -f1
}

find_not_owned_by_user() {
	find ! -user "${1:-$USER}"
}
