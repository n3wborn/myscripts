#!/bin/bash

srv="my-seedbox.domain.tld"
srv_user="user-on-my-seedbox"
srv_rt_watch_dir="private/rtorrent/watch/"
srv_rt_down_dir="private/rtorrent/data/"
srv_transm_watch_dir="private/transmission/watch/"

# send files to srv rtorrent watch dir
feral_rtorrent_push() {
	scp -v "$@" "${srv_user}"@"${srv}":"${srv_rt_watch_dir}"
}

# send most recent torrent in current dir
send_last_torrent() {
	files="$(ls -t *.torrent | head -n1)"

	for file in $files; do
		frp "${file}"
	done
}

# send files to srv transmission watch dir
feral_transmission_remote_push() {
	scp -V "$@" "${srv_user}"@"${srv}":"${srv_transm_watch_dir}"
}

# download a file from rtorrent datadir to my pc
feral_rtorrent_pull() {
	rsync \
		--human-readable \
		--verbose \
		--recursive \
		--partial \
		--progress \
		--rsh=ssh \
		"${srv_user}"@"${srv}":"${srv_rt_down_dir}""${1}" "${2}"
}
