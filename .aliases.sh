#!/bin/sh

# editors
alias v="vim"
alias n="nvim"

# set it utf-8
alias tmux="tmux -u"
alias ta="tmux attach"
alias tl="tmux list-sessions"
alias tn="tmux new -t"

# transmission easy
alias tdstart="transmission-daemon   --log-info --logfile /tmp/transmission.log --config-dir .config/transmission/"
alias tstart='transmission-remote -tall --start'
alias tstop='transmission-remote -tall --stop'
alias tactive='transmission-remote -tall --list | grep -Ei "active|seeding"'
alias tlist='transmission-remote -tall --list'
alias tsort='transmission-remote -t all --info | grep -E "Name:|Date added:"'

# rtorrent
alias frp='feral_rtorrent_push'
alias slt='send_last_torrent'

# bc, just quiet
alias bc='bc -q'

# ls the cool way
alias ls='ls -hN --color=auto --group-directories-first'

# screenlock
alias lock="xflock4"

# suspend to ram
alias str="lock && systemctl suspend"

# find files/dirs I don't own
# ${USER} will be expanded when defined, not when executing the alias
# so we escape it
alias find_files_not_owned_by_user="find . ! -user \${USER}"

# download a video file, extract audio with the best quality. Add a thumbnail
# and format resulting file as I want.
# https://github.com/ytdl-org/youtube-dl
alias yt-dlp-mp3="yt-dlp    \
    --verbose               \
    --continue              \
    --console-title         \
    --extract-audio         \
    --audio-format mp3      \
    --audio-quality 0       \
    --embed-thumbnail       \
    -o '%(title)s.%(ext)s'  \
    -- "

# rsync. Just add -e=ssh if rsync to/from a remote host
alias rsync="rsync --human-readable --verbose --recursive --partial  --progress"

# termbin (pastebin-like)
# Exemple:
# $ ifconfig | tb
# https://termbin.com/1op6
alias tb="nc termbin.com 9999"

# find disk eaters
alias diskspace="du -S | sort -n -r |more"

# every day git stuff
alias ga="git add"
alias gaa="git add ."
alias gbv="git branch -vv"
alias gcl="git clone"
alias gco="git checkout"
alias gc="git commit"
alias gd="git diff"
alias gdc="git diff --cached"
alias gds="git diff --staged"
alias gf="git fetch"
alias gl="git log --oneline --graph"
alias gm="git merge"
alias gp="git pull"
alias grv="git remote -v"
alias gs="git status"
alias grpo="git remote prune origin"
alias gcb="git-checkout-with-fzf.sh"
