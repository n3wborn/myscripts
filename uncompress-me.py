#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Note :
#
#   Ce script sert juste à satisfying-candy-4.sh.
#   L'entrée qu'il va traiter est une chaine de caractere
#   qui a ete encodee en base64 puis compressee par zlib.
#
#   Il va afficher sur stdout le resultat de l'operation inverse
#   (stdout sera recupere et renvoyé sur irc par satisfying-candy-4.sh)

import sys
import zlib
import base64

# Mini script python pour decompresse et decode une chaine
# Une fois "eJxzrHItCqn0zC8AABBiA2g=" decode, il doit valoir "AzErTyIop"

data = sys.argv[1]
answer = zlib.decompress(base64.b64decode(data)).decode()
print(answer)

