#!/bin/sh

# function-srv.sh
#
# notify if any host given as argument is unreachable

# add an usage message if needed
usage() {
	cat <<-EOF
		Usage :

		$0 host@domain.tld [host2@domain.tld]

	EOF
	exit 1
}

# check if usage is needed
[ $# -lt 1 ] && usage

# check each host responds to ping
for host in "${@}"; do
	nohup ping -c1 "${host}" 2>/dev/null ||
		notify-send --app-name serv-watch \
			--urgency=critical \
			Alert "${host} not responding"
done
