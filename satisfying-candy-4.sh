#!/bin/bash -

# https://www.root-me.org/fr/Challenges/Programmation/IRC-Uncompress-me

# Bon, pour entamer la conversation avec Candy, il faut se tenir pret.
# Dans un autre term/buffer, on commence par lui passer un coup de fil.
# $ ii -s irc.hackerzvoice.net -p 6667  -n justatest -f justatest
#
# README :
#   Attention à ne pas modifier les fifo's in/out sans quoi rien ne
#   sera transmis ou reçu par ii !!

# si besoin, on se presente
echo '/NickServ IDENTIFY motdepasse' >  /home/stef/irc/irc.hackerzvoice.net/in && echo "NickServ OK"

# on lui dit qu'on aimerait parler avec elle
echo '/j Candy !ep4' > /home/stef/irc/irc.hackerzvoice.net/in

# on lui laisse le temps de parler, mais pas trop ! on a que de quoi parler 2
# secondes, c'est peu
sleep 0.5

# on ecoute ce qu'elle a a dire
# et garde ca en tete
coded=$(tail -1 /home/stef/irc/irc.hackerzvoice.net/candy/out | grep -oE '[^ ]+$')

answer=$(python programmation--uncompress-me.py "${coded}")
mess="$(echo ${answer} | cut -d'%' -f1)"

# je verifie les chaines
#echo "coded = ${coded}"
#echo "answer = ${answer}"
#echo "mess = ${mess}"

# Satisfaisons Candy ! Et surtout, sachons y mettre mes formes !!
LC_ALL=C printf '/j Candy !ep4 -rep %s\n' "${mess}" > /home/stef/irc/irc.hackerzvoice.net/in
