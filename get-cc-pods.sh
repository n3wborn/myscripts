#!/bin/bash -

# script tout simple pour aller chercher les podcasts du site lescastcodeurs.com
#
# TODO:
#       1)
#       - verifier si on en a deja
#       - si oui, regarder si il y en a des nouveaux et telecharger ceux-ci uniquement
#       - si non, prends ce qu'il y a
#
#       2) Ajouter d'autres sources de podcasts et faire un script global
#
url='https://lescastcodeurs.com'

for file in $(curl -s "${url}" | grep -oE '"http.*.mp3"' | tr -d '"'); do
	wget -c --show-progress "${file}"
done
