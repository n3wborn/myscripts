#!/bin/bash -

# storm-storm.sh
#
# storm-storm.sh, named like that for "Script TO ReMove phpSTORM" will
# backup/wipe phpStorm install files (yeah, prefs too, that's the deal ftm)

FILES="${HOME}/.config/JetBrains ${HOME}/.local/share/JetBrains ${HOME}/.cache/JetBrains ${HOME}/.java/.userPrefs/prefs.xml ${HOME}/.java/.userPrefs/jetbrains"
EXE=$(basename "${0}")

usage() {
	cat <<EOF
Usage : ${EXE} [-h | --help] [-f | --force]
BACKUP or REMOVE phpStorm config files

Without options, ${EXE} will backup them.

Options :

    -h, --help      Show this help
    -f, --force     Remove without any backups

EOF
}

remove() {
	for target in ${FILES}; do
		rm "${target}" -rf &&
			printf '%s removed\n' "${target}"
	done
}

chk_in() {
	for target in ${FILES}; do
		if [ -f "${target}" ]; then
			continue
		else
			printf '%s not found !\n' "${target}"
			exit 1
		fi
	done
}

chk_out() {
	for target in ${FILES}; do
		if
			[ -f "${target}.bak" ]
		then
			printf '%s already exists !\n' "${target}.bak"
		fi
	done
}

move() {
	for target in ${FILES}; do
		mv "${target}"{,.bak} &&
			printf '%s -> %s\n' "${target}" "${target}.bak"
	done
}

if [ $# -gt 1 ]; then
	usage && exit 1
else
	case $1 in
	-h | --help) usage ;;
	-f | --force) chk_in && remove ;;
	*) chk_in && chk_out && move ;;
	esac
	exit $?
fi
