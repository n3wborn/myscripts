#!/bin/bash

ARR=''
ARR[1]="${HOME}/.functions-div.sh"
ARR[2]="${HOME}/.functions-docker.sh"
ARR[3]="${HOME}/.functions-seedbox.sh"
ARR[4]="${HOME}/.functions-virt.sh"

#Just source other functions related scripts
for i in "${ARR[@]}"; do
	echo "${i}"
done
